from flask import Flask, request, jsonify, json

def response(response={}, status=200, mimetype=None, page=1, lenght=1000, success=None):
    """
    Generate default json resonse for controller.
    :param response: data to return in request
    :param status: code of response example : 200 it`s mean response OK
    :param mimetype:
    :param page: information of with page of data is return
    :param lenght: how meny page is with data
    :return:
    """
    if status == 200:
        _success = True
    else:
        _success = False

    if success is not None:
        _success = success

    response = {
        'data': response,
        'success': _success
    }

    response = jsonify(response)
    response.status_code = status

    return response

app = Flask(__name__)

from .index import index

app.register_blueprint(index, url_prefix='/')