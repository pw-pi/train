from src.semaphores.semaphore import SemaphoreAbstract
from enum import Enum


class SemaphoreL8Type(SemaphoreAbstract):
    __green = None
    __red = None
    __orange = None
    __white = None
    
    def __init__ (self, signals = []):
        [self.__green, self.__red, self.__orange, self.__white] = signals
        super().__init__(signals)
        self.signal(SemaphoreL8Type.SIGNALS['S1'])

    def signal (self, signalType):
        print(signalType)
        self.reset()

        if signalType == SemaphoreL8Type.SIGNALS['S1']:
            self.s1()
        elif signalType == SemaphoreL8Type.SIGNALS['S2']:
            self.s2()
        elif signalType == SemaphoreL8Type.SIGNALS['S3']:
            self.s3()
        elif signalType == SemaphoreL8Type.SIGNALS['S10']:
            self.s10()
        elif signalType == SemaphoreL8Type.SIGNALS['S11']:
            self.s11()
        elif signalType == SemaphoreL8Type.SIGNALS['SZ']:
            self.sz()
        elif signalType == SemaphoreL8Type.SIGNALS['M']:
            self.m()
        else:
            self.s1()

    # Stop
    def s1 (self):
        self.setSignal(self.__red)

    # Jazda z najwyższą dozwoloną prędkością
    def s2 (self):
        self.setSignal(self.__green)
    
    # Jazda z największą dozwoloną prędkością - w przodzie są dwa odstępy blokowe wolne (jeżeli jest to semafor wyjazdowy lub odstępowy na szlaku wyposażonym w samoczynną blokadę liniową, nie dotyczy ostatniego semafora odstępowego samoczynnej blokady liniowej ze wskaźnikiem W18) - albo przy następnym semaforze z prędkością nie większą niż 100 km/h (jeżeli sygnał jest wyświetlony na ostatnim semaforze samoczynnej blokady liniowej ze wskaźnikiem W18, semaforze wjazdowym lub drogowskazowym)
    def s3 (self):
        self.setSignal(self.__green)
        self.startBlink(self.__green)

    # Jazda z prędkością nie przekraczającą 40 km/h, a przy następnym z najwyższą dozwoloną prędkością.
    def s10 (self):
        self.setSignal(self.__green, self.__orange)

    # Jazda self.__greenz prędkością nie przekraczającą 40 km/h, a przy następnym semaforze z prędkością zmniejszoną do 100 km/h.
    def s11 (self):
        self.setSignal(self.__orange)
        self.startBlink(self.__green)

    # Sygnał zastępczy
    def sz (self):
        self.setSignal(self.__red)
        self.startBlink(self.__white)

    # Jazda manewrowa
    def m (self):
        self.setSignal(self.__white)