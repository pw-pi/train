import configparser
from datetime import datetime
from src.semaphores.tm import SemaphoreTmType
from src.semaphores.l7 import SemaphoreL7Type
from src.semaphores.l8 import SemaphoreL8Type
from src.semaphores.semaphore import GPIO

GPIO.setmode(GPIO.BCM)

devicesObj = {}

devices = configparser.ConfigParser()
devices.read('devices.ini')

print(list(devices))

def __createDeviceObject (device):
    global devicesObj

    if device['type'] == 'l7':
        devicesObj[device['id']] = SemaphoreL7Type([device['green'], device['red'], device['white']])
        devicesObj[device['id']].setDaemon(True)
        devicesObj[device['id']].start()
    elif device['type'] == 'l8':
        devicesObj[device['id']] = SemaphoreL8Type([device['green'], device['red'], device['orange'], device['white']])
        devicesObj[device['id']].setDaemon(True)
        devicesObj[device['id']].start()
    elif device['type'] == 'l8':
        devicesObj[device['id']] = SemaphoreTmType([device['blue'], device['white']])
        devicesObj[device['id']].setDaemon(True)
        devicesObj[device['id']].start()
    # elif device['type'] == 'zw-sg':
    #     device[device['id']] = SteeringSgType(device['signal'])

    devicesObj[device['id']].id = device['id']

    print(devicesObj)


for device in devices:
    try:
        __createDeviceObject(devices[device])
    except:
        pass

