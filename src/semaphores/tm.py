from src.semaphores.semaphore import SemaphoreAbstract
from enum import Enum

class SemaphoreTmEnum (Enum):
    S1 = 's1'
    S2 = 's2'
    S3 = 's3'
    SZ = 'sz'
    M = 'm'

class SemaphoreTmType (SemaphoreAbstract):
    __blue = None
    __white = None
    
    def __init__ (self, signals = []):
        [self.__blue, self.__white] = signals
        super().__init__(signals)
        self.signal(SemaphoreTmEnum.S1)
    
    def signal (self, signalType):
        if signalType == SemaphoreTmEnum.S1 or signalType == 's1':
            self.s1()
        elif signalType == SemaphoreTmEnum.M or signalType == 'm':
            self.m()
        else:
            self.s1()

    def s1 (self):
        """ Sygnał S1 stop"""
        self.setSignal(self.__blue)

    def m (self):
        """ Sygnał Jazda manewrowa dozwolona """
        self.setSignal(self.__white)