#!/bin/python3

import src.devices

from src import core
from src.semaphores.controller import semaphor

core.app.register_blueprint(semaphor)

app = core.app
