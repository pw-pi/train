import threading
from abc import ABC, abstractmethod
from datetime import datetime, timedelta

try:
    import RPi.GPIO as GPIO
except:
    class GPIO (object):
        """
        MOCK dla RPi GPIO
        """

        OUT = "out"
        BCM = "bcm"
        BOARD = "board"
        LOW = 0
        HIGHT = 1
        HIGH = 1
        
        @staticmethod
        def setup (pin, typeSignal, initial=None):
            print("PIN SETUP: " + str(pin))
            print(str(typeSignal))

        @staticmethod
        def setmode (typeSignal):
            print("MODE: " + str(typeSignal))

        @staticmethod
        def output (pin, typeSignal):
            print("PIN OUT: " + str(pin))
            print(str(typeSignal))

        def setwarnings (warnings = True):
            print("WARNINGS:" + str(warnings) + "\n")

class SemaphoreAbstract(ABC, threading.Thread):
    """
    Abstrakcyjna klasa do obsługi dowolnego typu semaforu.
    
    Atrybuty
    --------
    id: str
        Id urządzenia
    isSetSignal: number
        ustawiony sygnał na urządzeniu

    Metody
    --------

    reset()
        Przywracanie ustawień początkowych urządzenia
    
    run()
        Uruchamianie wątku

    signal(type)
        Ustawianie sygnału dla urządzenia bo typie

    startBlink(pin)
        Uruchaminanie migania na podanym sygnale

    setSignal(pin, pin2=None)
        Ustawianie sygnału lub drugiego sygnału na urządzeniu
    """

    SIGNALS = {
        'S1': 's1',
        'S2': 's2',
        'S3': 's3',
        'S10': 's10',
        'S11': 's11',
        'SZ': 'sz',
        'M': 'm'
    }

    id = None
    isSetSignal = None

    __blink = False
    __pinBlink = None
    __pins = []
    __signal = None
    __timeset = None

    def __init__ (self, pins=[]):
        self.__pins = pins
        threading.Thread.__init__(self)

    def reset (self):
        self.__stopBlink()
        
        for pin in self.__pins:
            self.__off(pin)

    def run (self):
        self.__run()

    @abstractmethod
    def signal (self, type):
        pass

    def startBlink (self, pin):
        self.__pinBlink = pin
        self.__off(self.__pinBlink)

        if not self.__timeset:
            self.__timeset = datetime.now() + timedelta(seconds=1)

    def setSignal (self, pin, pin2=None):
        self.__setUpPin()
        self.__stopBlink()
        self.__signal = int(pin)

        if pin2:
            self.__on(pin2)

    def __on (self, pin): 
        GPIO.output(int(pin), GPIO.HIGH)

    def __off (self, pin): 
        GPIO.output(int(pin), GPIO.LOW)

    def __run (self):
        while True:
            if self.__timeset  and self.__timeset < datetime.now():
                self.__triggerBlink()
                self.__timeset = datetime.now() + timedelta(seconds=1)
            
            if self.__signal != self.isSetSignal:
                self.isSetSignal = self.__signal
                self.__on(self.isSetSignal)

    def __setUpPin (self):
        for pin in self.__pins:
            GPIO.setup(int(pin), GPIO.OUT, initial=GPIO.LOW)
    
    def __stopBlink (self):
        self.__timeset = None
        self.__blink = False
        self.isSetSignal = None
        
        if self.__pinBlink:
            self.__off(self.__pinBlink)

    def __triggerBlink (self):
        if self.__blink:
            self.__blink = False
            self.__off(self.__pinBlink)
        else:
            self.__blink = True
            self.__on(self.__pinBlink)
