from src import semaphores
from src.core import response
from src.devices import devicesObj
from flask import Blueprint


semaphor = Blueprint('semaphor', __name__, url_prefix='/semaphor')

@semaphor.route ('/<id>', methods=['GET'])
def get_signal(id):
    print('id: ' + id)
    print(devicesObj[id].id)
    print(devicesObj[id].isSetSignal)
    try:
        return response({'id': devicesObj[id].id, 'signal': devicesObj[id].isSetSignal})
    except:
        return response(None)

@semaphor.route('/<id>/<signal>', methods=['PATCH', 'GET'])
def patch_signal (id, signal):
    try:
        devicesObj[id].signal(signal)
        return response({'id': devicesObj[id].id, 'signal': devicesObj[id].isSetSignal})
    except:
        return response(response='OK')
